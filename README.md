# Network Automation 101 Workshop

# Why Automate?

* Digital transformation – Moving workloads between Private and Public Clouds
* Organizations are modernizing their business strategies and technologies to better support customer needs
* As companies embrace digital transformation, they need to move at the pace of software, new apps, new clouds and new technologies
* Traditional networking engineering and administration often do not align with those requirements

#### By adopting network automation organizations can take advantage of the following:

### Standardized Processes

* Reduced chance of human errors, which affect network performance and security
* Eliminate manual network tasks and enhance network functionality

### Robust Quality Control

*  Integrated testing
*  Ability to locate and identify the root cause of network or security issues before deploying to production
  
### Speed and Agility

*  Continuous Improvement
*  Rapid provisioning of new services, self service provisioning, enhancements, and upgrades

# What's The Fear?

### Culture change
*  Organization commitment
*  Top down, bottom up

### Willingness to learn new things
*  Takes 15 minutes for me to do it, it will take me hours to automate
  
## Machines should do repetitive work, people should do problem solving!

# The Network Automation Workshop

*  We have designed a network automation workshop to help network engineers become familiar with the technology
*  To overcome some of the fears

#### We have broken it down into the following stages:

![image info](infra/images/ind-1.png)

# 4 Stages

In each stage we introduce new concepts and technology that gradually build on the prior step

![image info](infra/images/ind-2.png)

# Network Automation Technology

### All the technology included in the workshop

Network Engineers and Architects will work on the following platforms: 

* Visual Studio Code
* Git
* Docker
* ContainerLab
* Ansible
* Gitlab
* Python
* APIs
* CI/CD
* Batfish
* Nautobot

### Network Diagram

Two Ubuntu Linux Server will be required to support the workshop

![image info](infra/images/ind-3.png)

# Network Automation Workflow

During the workshop, Network Engineers and Architects will have the opportunity to become familiar with some of the following technology:

![image info](infra/images/ind-4.png)

# Summary

By the end of the workshop.  The Network Engineer and Architect will gain the confidence to adopt network automation by implementing the following pipeline:

![image info](infra/images/ind-5.png)

### For more information on how to participate in the workshop 

#### Contact Ken Norton
##### <knorton@presidio.com>
##### [@knorton3](https://twitter.com/knorton3)
